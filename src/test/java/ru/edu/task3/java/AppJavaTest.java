package ru.edu.task3.java;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * ReadOnly
 */
public class AppJavaTest {

    public static final String PROM = "PROM";
    public static final String DEBUG = "DEBUG";

    @Test
    public void run() {
        assertEquals(PROM, AppJava.run(PROM).getValue());
        assertEquals(DEBUG, AppJava.run(DEBUG).getValue());
    }
}