package ru.edu.cyclic;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Registry {

    @Autowired
    private List<Element> elements = new ArrayList<>();


//    public Registry(List<Element> list) {
//       elements.addAll(list);
//    }

    public List<Element> getElements() {
        return elements;
    }
}
